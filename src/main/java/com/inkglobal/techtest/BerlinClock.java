package com.inkglobal.techtest;

public class BerlinClock {

	public String getSecondsBlink(int i) {
		if(i % 2 == 0){
			return "Y";
		}
		else return "O";
	}

	public String getFirstHour(int i) {
		int onLamps = (i - (i % 5)) / 5;		
		return calculateLampStatus(4, onLamps, "R");
	}
	
	private String calculateLampStatus(int numOfLamps, int onLamps, String sign) {
		String time = "";
		for(int i = 0; i < onLamps; i++){
			time += sign;
		}
		for(int i = 0; i < (numOfLamps - onLamps); i++) {
			time += "O";
		}
		return time;
	}

	public String getSecondHour(int i) {
		int onLamps = i % 5;
		
		return calculateLampStatus(4, onLamps, "R");
	}

	public String getFirstMinutes(int i) {
		int onLamps = i / 5; 
		
		return calculateLampStatus(11, onLamps, "Y").replace("YYY", "YYR");
	}

	public String getSecondMinutes(int i) {
		int onLamps = i % 5; 
		
		return calculateLampStatus(4, onLamps, "Y");
	}

	public String[] convertToBerlinTime(String gTime) {
		String[] time = gTime.split(":");
		int hours = Integer.parseInt(time[0]);
		int minutes = Integer.parseInt(time[1]);
		int seconds = Integer.parseInt(time[2]);		
		
		return new String[] {getSecondsBlink(seconds),
							getFirstHour(hours),
							getSecondHour(hours),
							getFirstMinutes(minutes),
							getSecondMinutes(minutes)};
	}
	
	
}
