package com.inkglobal.techtest;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BerlinClockTest {
	BerlinClock berlinClock = new BerlinClock() ;
	
	
	//Test 2 seconds blink
	@Test
	public void testSecondsBlinking() {
		assertEquals("O", berlinClock.getSecondsBlink(3));
		assertEquals("Y", berlinClock.getSecondsBlink(0));
	}
	
	//Test first/ top row of hours lamps
	@Test 
	public void testFirstHourRow() {
		assertEquals("OOOO", berlinClock.getFirstHour(0));
		assertEquals("ROOO", berlinClock.getFirstHour(7));
		assertEquals("RROO", berlinClock.getFirstHour(14));
		assertEquals("RRRR", berlinClock.getFirstHour(20));
	}
	
	//Test second/ bottom of hours lamps
	@Test
	public void testSecondHourRow() {
		assertEquals("ROOO", berlinClock.getSecondHour(1));
		assertEquals("RRRO", berlinClock.getSecondHour(3));
	}
	
	///Test first/ top row of minutes lamps
	@Test
	public void testFirstMinutesRow(){
		assertEquals("YYRYYROOOOO", berlinClock.getFirstMinutes(30));
	}
	
	//Test second/ bottom of minutes lamps
	@Test
	public void testSecondMinutesRow(){
		assertEquals("OOOO", berlinClock.getSecondMinutes(0));
		assertEquals("YYYO", berlinClock.getSecondMinutes(3));
	}
	
	// Convert 00:00:00 to Berlin time
	@Test
	public void getBerlinTime1() {
		String gTime = "00:00:00";
		String[] bTime = berlinClock.convertToBerlinTime(gTime);
		
		String[] expTime = new String[]{"Y", "OOOO", "OOOO", "OOOOOOOOOOO", "OOOO"};		
		System.out.println(bTime[0] + "\n" + bTime[1] + "\n" + bTime[2] + "\n" + bTime[3] + "\n" + bTime[4]);
		assertEquals(expTime.length, bTime.length);
		
		for(int i=0; i < expTime.length; i++){
			assertEquals(expTime[i], bTime[i]);
		}
	}
	
	// Convert 13:17:01 to Berlin time
	@Test
	public void getBerlinTime2() {
		String gTime = "13:17:01";
		String[] bTime = berlinClock.convertToBerlinTime(gTime);
		
		String[] expTime = new String[]{"O", "RROO", "RRRO", "YYROOOOOOOO", "YYOO"};		
		System.out.println("\n" + bTime[0] + "\n" + bTime[1] + "\n" + bTime[2] + "\n" + bTime[3] + "\n" + bTime[4]);
		assertEquals(expTime.length, bTime.length);
		
		for(int i=0; i < expTime.length; i++){
			assertEquals(expTime[i], bTime[i]);
		}
	}
	
	// Convert 23:59:59 to Berlin time
	@Test
	public void getBerlinTime3() {
		String gTime = "23:59:59";
		String[] bTime = berlinClock.convertToBerlinTime(gTime);
		
		String[] expTime = new String[]{"O", "RRRR", "RRRO", "YYRYYRYYRYY", "YYYY"};		
		System.out.println("\n" + bTime[0] + "\n" + bTime[1] + "\n" + bTime[2] + "\n" + bTime[3] + "\n" + bTime[4]);
		assertEquals(expTime.length, bTime.length);
		
		for(int i=0; i < expTime.length; i++){
			assertEquals(expTime[i], bTime[i]);
		}
	}
	
	// Convert 24:00:00 to Berlin time
	@Test
	public void getBerlinTime4() {
		String gTime = "24:00:00";
		String[] bTime = berlinClock.convertToBerlinTime(gTime);
		
		String[] expTime = new String[]{"Y", "RRRR", "RRRR", "OOOOOOOOOOO", "OOOO"};		
		System.out.println("\n" + bTime[0] + "\n" + bTime[1] + "\n" + bTime[2] + "\n" + bTime[3] + "\n" + bTime[4]);
		assertEquals(expTime.length, bTime.length);
		
		for(int i=0; i < expTime.length; i++){
			assertEquals(expTime[i], bTime[i]);
		}
	}
	
}
